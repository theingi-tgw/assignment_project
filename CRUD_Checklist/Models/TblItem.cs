﻿using System;
using System.Collections.Generic;

namespace CRUD_Checklist.Models
{
    public partial class TblItem
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string ItemName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool? Status { get; set; }
        public byte[] ItemImage { get; set; }

        public virtual TblCategory Category { get; set; }
    }
}
