﻿using System;
using System.Collections.Generic;

namespace CRUD_Checklist.Models
{
    public partial class TblCategory
    {
        public TblCategory()
        {
            TblItem = new HashSet<TblItem>();
        }

        public int Id { get; set; }
        public string Category { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreatedBy { get; set; }

        public virtual ICollection<TblItem> TblItem { get; set; }
    }
}
