﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRUD_Checklist.Models;
using Microsoft.AspNetCore.Mvc;

namespace CRUD_Checklist.Controllers
{
    public class RecordController : Controller
    {
        public  readonly MainDBContext _context;

        public RecordController(MainDBContext context)
        {
            _context = context;
        }


        public IActionResult Index()
        {
            List<Record> records = new List<Record>();

            records =  (from t1 in _context.TblCategory
                       join t2 in _context.TblItem on t1.Id equals t2.CategoryId
                       select new Record
                       {
                           Category = t1.Category,
                           Item = t2.ItemName,
                           CreatedBy = t2.CreatedBy,
                           CreatedDate = t2.CreatedDate,
                       }).OrderBy(t2 => t2.CreatedDate).ToList();

            return View(records);
        }
    }
}