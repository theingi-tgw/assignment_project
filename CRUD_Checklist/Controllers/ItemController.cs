﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CRUD_Checklist.Models;
using Microsoft.AspNetCore.Http;

namespace CRUD_Checklist.Controllers
{
    public class ItemController : Controller
    {
        private readonly MainDBContext _context;

        public ItemController(MainDBContext context)
        {
            _context = context;
        }

        // GET: Item
        public async Task<IActionResult> Index()
        {
            var mainDBContext = _context.TblItem.Include(t => t.Category);
            return View(await mainDBContext.ToListAsync());
        }

        // GET: Item/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblItem = await _context.TblItem
                .Include(t => t.Category)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblItem == null)
            {
                return NotFound();
            }

            return View(tblItem);
        }

        // GET: Item/Create
        public IActionResult Create()
        {
            ViewData["CategoryId"] = new SelectList(_context.TblCategory, "Id", "Category");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(TblItem tblItem, IFormFile files)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tblItem);
                tblItem.CreatedDate = DateTime.Today;
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(_context.TblCategory, "Id", "Category", tblItem.CategoryId);
            return View(tblItem);
        }

        // GET: Item/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblItem = await _context.TblItem.FindAsync(id);
            if (tblItem == null)
            {
                return NotFound();
            }
            ViewData["CategoryId"] = new SelectList(_context.TblCategory, "Id", "Category", tblItem.CategoryId);
            return View(tblItem);
        }

        // POST: Item/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,CategoryId,ItemName,CreatedBy,CreatedDate,Status,ItemImage")] TblItem tblItem)
        {
            if (id != tblItem.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tblItem);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TblItemExists(tblItem.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(_context.TblCategory, "Id", "Category", tblItem.CategoryId);
            return View(tblItem);
        }

        // GET: Item/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblItem = await _context.TblItem
                .Include(t => t.Category)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblItem == null)
            {
                return NotFound();
            }

            return View(tblItem);
        }

        // POST: Item/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tblItem = await _context.TblItem.FindAsync(id);
            _context.TblItem.Remove(tblItem);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblItemExists(int id)
        {
            return _context.TblItem.Any(e => e.Id == id);
        }
    }
}
